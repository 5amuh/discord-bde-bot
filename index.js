// ----- Load libraries and other files -----

// Load up external libraries
const Discord = require('discord.js');
const fs = require('fs');

// The client, the bot itself
const client = new Discord.Client();

// Load the token file that contains the bot's token
const { token } = require(`${__dirname}/token.json`);

// The file with several config constants
client.config = require(`${__dirname}/config.js`);


// ----- Load command modules -----

// Function to load the commands
const loadCommands = function loadCommands() {
    const commands = {};
    const files = fs.readdirSync(client.config.commandsDirectory);

    files.forEach((file) => {
        if (file.endsWith('.js')) {
            commands[file.slice(0, -3)] = require(`${client.config.commandsDirectory}/${file}`);
            console.log(`Loaded ${file}`);
        }
    });

    return commands;
};

// Load all command modules available in commands directory
client.commands = loadCommands();


// ----- Change bot behaviour depending on events -----

// This event will run if the bot starts, and logs in, successfully
client.on('ready', () => {
    console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`);

    // Changing the bot's playing game
    client.user.setActivity(client.config.activityMessage, { type: 'PLAYING' })
        .catch(console.error);
});

// This event will run whenever the bot reads a message (on a channel or a PM)
client.on('message', (message) => {
    // Ignore message if it does not begin with prefix, or if  it was sent by a bot
    if (!message.content.startsWith(`${client.config.prefix}`) || message.author.bot) {
        return;
    }

    // Here we separate our "command" name, and our "arguments" for the command.
    // e.g. if we have the message "+say Is this the real life?" , we'll get the following:
    // command = say
    // args = ["Is", "this", "the", "real", "life?"]
    const args = message.content.slice(client.config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // if the command asked exists, execute it
    if (Object.keys(client.commands).includes(command)) {
        client.commands[command].run(client, message, args);
    }
});

// This event will run whenever someone updates their message (on a channel or a PM)
client.on('messageUpdate', (oldMessage, newMessage) => {
    // Ignore message if it does not begin with prefix, or if  it was sent by a bot
    if (!newMessage.content.startsWith(`${client.config.prefix}`) || newMessage.author.bot) {
        return;
    }

    // Here we separate our "command" name, and our "arguments" for the command.
    // e.g. if we have the message "+say Is this the real life?" , we'll get the following:
    // command = say
    // args = ["Is", "this", "the", "real", "life?"]
    const args = newMessage.content.slice(client.config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // if the command asked exists, execute it
    if (Object.keys(client.commands).includes(command)) {
        client.commands[command].run(client, newMessage, args);
    }
});

// this event will run whenever a member leaves the guild, or is kicked
client.on('guildMemberRemove', (member) => {
    // Channel in which we log people quitting the server.
    const chanLogQuit = client.channels.get(client.config.idChanLogQuit);

    const pseudo = member.displayName;
    const mention = `<@${member.user.id}>`;
    chanLogQuit.send(`\`${pseudo}\` (${mention}) vient de quitter le serveur.`)
        .catch(console.error);
});

// this event will run whenever a member leaves the guild, or is kicked
client.on('guildMemberUpdate', (oldMember, newMember) => {
    // x seconds timeout
    setTimeout(() => {
        const mention = `<@${newMember.user.id}>`;

        // If the member was just given the role 0A
        if (!oldMember.roles.has(client.config.idRole0A) && newMember.roles.has(client.config.idRole0A)) {
            newMember.send(`Coucou ${mention}, bienvenue sur le serveur Discord du BdE de l'ENSIIE ! Maintenant que tu as le rôle 0A, tu as accès aux chans suivants :
- <#${client.config.idChanSpam0A}> : un chan de spam où tu peux discuter de tout et de rien aussi bien avec d'autres 0A qu'avec les élèves de l'école.
- <#${client.config.idChanSerieux0A}> : si tu as des questions plus sérieuses sur l'école, les cours..., c'est là qu'il faudra les poser :wink:
- <#${client.config.idChanLogement0A}> : pareil qu'au dessus, mais pour tout ce qui concerne les logements (résidences étudiantes, recherche de coloc...)
- <#${client.config.idChanPresentations0A}> : pour te présenter, tout simplement :smiley:

Je parle de 0A depuis tout à l'heure : ce terme désigne quelqu'un pas encore à l'école, donc soit qui se renseigne, soit qui sait déjà qu'il va intégrer l'ENSIIE.
D'ailleurs, il y a un chan pour que vous puissiez parler entre 0A : <#${client.config.idChanOnly0A}>

N'hésite pas à venir dire bonjour :wave: La plupart des iiens est en stage donc s'ennuie et ça peut spam pas mal, mais on ne mord pas :angel:
`).catch(console.error);
        }// endif
    }, 5 * 1000);
});


client.login(token);

