module.exports.prefix = '.';

module.exports.commandsDirectory = `${__dirname}/commands`;

module.exports.activityMessage = `${this.prefix}help`;

// the channel the bot will read and delete messages
module.exports.authorizedChans = ['451696544085704704'];

module.exports.removeMessageRole = true;

module.exports.idChanLogQuit = '451685969716838402';

module.exports.idChanOnly0A = '592779322226507776';

module.exports.idChanSpam0A = '552105790379524097';

module.exports.idChanSerieux0A = '592779142961823765';

module.exports.idChanLogement0A = '595585655275257856';

module.exports.idRole0A = '449295406459781132';

module.exports.idChanPresentations0A = '552105545218260992';

/*
- In aliases array, only write in lower case (so that we don't need to apply the toLowerCase function to each alias array)
- id should be a string, because it would be too big as an int
*/
const roles = {
    Muzzik: {
        id: '441554528324550666',
        alias: [],
    },
    CCCM: {
        id: '441362953774825483',
        alias: ['fanfare'],
    },
    Liien: {
        id: '441352757123874816',
        alias: [],
    },
    'i-TV': {
        id: '441531527193427988',
        alias: ['itv'],
    },
    Siieste: {
        id: '441370359191568384',
        alias: [],
    },
    Bakaclub: {
        id: '441356700168224784',
        alias: ['baka', 'bakateux'],
    },
    LeBarC: {
        id: '441360682789634048',
        alias: ['bar', 'barman'],
    },
    RealItIE: {
        id: '441371485869703198',
        alias: [],
    },
    Arise: {
        id: '441363462552027146',
        alias: [],
    },
    LanPartIIE: {
        id: '442764349983948801',
        alias: ['lp'],
    },
    FiLiGRANe: {
        id: '441523099851292672',
        alias: ['magic'],
    },
    Dièse: {
        id: '441368522623287296',
        alias: ['#'],
    },
    GuIIldE: {
        id: '441538039756488725',
        alias: ['jdr'],
    },
    ÉcologIIE: {
        id: '441893117277372416',
        alias: ['ecologiie'],
    },
    Gala: {
        id: '444971991582244864',
        alias: [],
    },
    Nightiies: {
        id: '448900305480974336',
        alias: ['dj'],
    },
    RadIoactIvE: {
        id: '441557454736719873',
        alias: ['radio'],
    },
    CuIsInE: {
        id: '441367326823153674',
        alias: [],
    },
    CID: {
        id: '441522926609498114',
        alias: [],
    },
    DansIIE: {
        id: '441357296916889600',
        alias: [],
    },
    ForumIIE: {
        id: '441561935620734988',
        alias: ['forum'],
    },
    Houdiniie: {
        id: '441530506106896387',
        alias: [],
    },
    VocalIIsE: {
        id: '451705865649782794',
        alias: [],
    },
    PomPom: {
        id: '451706037184233492',
        alias: [],
    },
    InsanitIIE: {
        id: '451994637687848971',
        alias: [],
    },
    LumIIEre: {
        id: '458265769600352266',
        alias: [],
    },
    BDSF: {
        id: '458265466864140309',
        alias: ['bd-sf'],
    },
    IImondE: {
        id: '458020017200693248',
        alias: [],
    },
    Alcoolique: {
        id: '612927830757474314',
        alias: [],
    },
};
module.exports.roles = roles;
