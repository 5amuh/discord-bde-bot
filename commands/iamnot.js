exports.run = (client, message, args) => {
    // Retrieve parameters
    const { roles, authorizedChans, removeMessageRole } = client.config;

    // If the message was not sent in an authorized chan, stop
    if (!authorizedChans.includes(message.channel.id)) {
        return;
    }

    // For each role the user wants to get
    args.forEach((roleWanted) => {
        // For each role available in the config.js file
        Object.keys(roles).forEach((roleAvailable) => {
            // If the role wanted exists
            if (
                roleAvailable.toLowerCase() === roleWanted.toLowerCase()
                || roles[roleAvailable].alias.includes(roleWanted.toLowerCase())
            ) {
                // Add role to user
                const roleId = roles[roleAvailable].id;
                const roleToAddGuild = message.guild.roles.get(roleId);
                message.member.removeRole(roleToAddGuild).catch(console.error);
            }
        });
    });

    // Potentially delete the message
    if (removeMessageRole && message.deletable) {
        message.delete().catch(console.error);
    }
};


// Prepare help message

// Retrieve roles in config file (used for help too)
const { roles } = require(`${__dirname}/../config.js`);

const rolesWithAliases = Object.keys(roles).map((role) => {
    const aliases = roles[role].alias;
    if (aliases.length > 0) {
        return `${role} (${aliases.join(', ')})`;
    }
    return role;
});

const helpUsage = `iamnot <role1> [<role2> <role3> ...]\n
Roles available :
- ${rolesWithAliases.sort().join(`
- `)}`;

exports.help = {
    name: 'iamnot',
    category: 'Roles management',
    description: 'To remove roles to yourself.',
    usage: helpUsage,
};
