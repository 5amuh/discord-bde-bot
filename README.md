# Install modules listed as dependencies in package.json

- `npm install` will install all modules listed as dependencies in package.json. This is what you want to type when developping the app.
- `npm install --production` will not install modules listed in devDependencies. If you just want to use the bot, and not change the files content, type this.

# Files needed not in this git repo

- You need a `token.json` file in this folder, and you need to put your bot token inside. See `token.json.example` for example.

# Launch the bot

 Once you've done the two steps above (install modules and add the 2 files), you can start the bot by typing `node index.js`.
